import Pages from 'vite-plugin-pages'

/**
 * vite-plugin-pages
作用
vite-plugin-pages可以读取指定的目录文件，自动化生成路由信息，不需要自己去逐个页面配置

注意
1、vite-plugin-pages基于vue-router，所以使用的时候还是要安装vue-router
2、vite-plugin-pages默认指定的页面文件夹是 pages，默认指定的页面是 index.vue，所以最好先在pages文件夹下面创建一个 index.vue文件

src/
    ├── features/
    │  └── dashboard/
    │     ├── code/
    │     ├── components/
    │     └── pages/
    ├── admin/
    │   ├── code/
    │   ├── components/
    │   └── pages/
    └── pages/
    配置
    Pages({
        dirs: [
            { dir: 'src/pages', baseRoute: '' },
            { dir: 'src/features//pages', baseRoute: 'features' },
            { dir: 'src/admin/pages', baseRoute: 'admin' },
        ],
    })
路由规则：
基本路由:
src/pages/users.vue -> /users
src/pages/users/profile.vue -> /users/profile
src/pages/settings.vue -> /settings

索引路由:
src/pages/index.vue -> /
src/pages/users/index.vue -> /users

动态路由:
src/pages/users/[id].vue -> /users/:id (/users/one)
src/pages/[user]/settings.vue -> /:user/settings (/one/settings)

 */
export function createPages() {
    return Pages({
        dirs: [
            { dir: 'src/views', baseRoute: '' }
        ],
        importMode: "async"
        // exclude: [
        //     '**/components/**/*.vue'
        // ]
    })
}