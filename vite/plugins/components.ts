import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
import IconsResolver from 'unplugin-icons/resolver'

export function createComponents(isBuild: boolean) {


    const resolvers = []
    resolvers.push(ElementPlusResolver())
    resolvers.push(IconsResolver({
        enabledCollections: ['ep'],
    }))

    
    return Components({
        resolvers,
        dirs: ['src/components'],
        // include: [/\.vue$/, /\.vue\?vue/, /\.jsx$/],
        dts: 'src/components.d.ts'
    })
}