import { createSvgIconsPlugin } from 'vite-plugin-svg-icons'
import path from 'path';

export function createSvgIcon(isBuild: boolean) {

    return createSvgIconsPlugin({
        iconDirs: [path.resolve(process.cwd(), 'src/assets/icons/svg/')],
        symbolId: 'icon-[dir]-[name]',
        svgoOptions: isBuild
    })

}
