import vue from '@vitejs/plugin-vue'
import type { Plugin } from 'vite';
import {createComponents} from './components'
import {createAutoImport} from './auto-import'
import {createSvgIcon} from './svg-icon'
import {createPages} from './pages'
import {createLayouts} from './layouts'

export function createVitePlugins(viteEnv: any, isBuild = false) {
    const vitePlugins: (Plugin | Plugin[])[] = [
        vue()
    ]
    vitePlugins.push(createAutoImport())
    vitePlugins.push(createComponents(isBuild))
    vitePlugins.push(createSvgIcon(isBuild))
    vitePlugins.push(createPages())
    vitePlugins.push(createLayouts())

    
    return vitePlugins
}