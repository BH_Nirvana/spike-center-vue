import AutoImport from "unplugin-auto-import/vite";
import { ElementPlusResolver } from "unplugin-vue-components/resolvers";
import IconsResolver from "unplugin-icons/resolver";

export function createAutoImport() {
    return AutoImport({
        // 自动导入vue，vue-router里的绝大部分API，不需要import { useRouter } from 'vue-router'
        // 自动导入 Vue 相关函数，如：ref, reactive, toRef 等
        imports: [
            "vue",
            "vue-router",
            // {
            //     vue: ["defineProps", "defineEmits", "defineExpose", "withDefaults"],
            // },
            "pinia",
        ],
        eslintrc: {
            enabled: true,
            filepath: './.eslintrc-auto-import.json',
            globalsPropValue: true
        },

        dts: 'src/auto-import.d.ts',

        resolvers: [
            // 自动导入 Element Plus 相关函数，如：ElMessage, ElMessageBox... (带样式)
            ElementPlusResolver(),
            // 自动导入图标组件
            IconsResolver({
                prefix: "Icon",
            }),
        ],

    });
}
