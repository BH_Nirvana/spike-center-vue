/*
 * @Author: your name
 * @Date: 2021-07-23 11:16:43
 * @LastEditTime: 2021-07-26 10:29:14
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \digital_open_platform_main\src\utils\validate.js
 */
/**
 * Created by PanJiaChen on 16/11/18.
 */

/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal(path: string) {
  return /^(https?:|mailto:|tel:)/.test(path)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
// export function validUsername(str) {
//   const valid_map = ['admin', 'editor']
//   return valid_map.indexOf(str.trim()) >= 0
// }
