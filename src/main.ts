import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import router from './router/index'
import pinia from './store'
import axios from 'axios';
import VueAxios from 'vue-axios';

// 注入svg
import svgIcon from './components/SvgIcon/index.vue';
import 'virtual:svg-icons-register';

const app = createApp(App)
app.component('SvgIcon', svgIcon);
app.use(ElementPlus)
app.use(router)
app.use(pinia)
app.use(VueAxios,axios)
app.mount('#app')
