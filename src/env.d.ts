/// <reference types="vite/client" />
//注释：主要是加入下面这行，否则main.ts页面会报红
/// <reference types="vite-plugin-pages/client" />
interface ImportMetaEnv {
    readonly VITE_APP_TITLE: string
    readonly VITE_APP_API_BASEURL: string
    readonly VITE_OPEN_PROXY: boolean
  }
  
  interface ImportMeta {
    readonly env: ImportMetaEnv
  }
  
  declare module '*.vue' {
    import type { DefineComponent } from 'vue'
    // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
    const component: DefineComponent<{}, {}, any>
    export default component
  }
  