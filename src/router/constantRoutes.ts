import Layout from "@/layout/index.vue"
const constantRoutes = [
    {
        path:'/',
        redirect: '/home',
    },
    {
        path:'/home',
        name:'Home',
        component: Layout,
        redirect: '/home/home',
        children: [
            {
            path: 'home',
            name: '功能组件',
            component: () => import('@/views/home.vue'),
            meta: { 
                title: '功能组件', 
                icon: 'table' ,
            }
            },
        ]
    },


];

export default constantRoutes