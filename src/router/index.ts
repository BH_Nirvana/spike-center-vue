import {createRouter,createWebHistory,createWebHashHistory,RouteRecordRaw,} from "vue-router";
import constantRoutes from './constantRoutes'
import mainRoutes from './mainRoutes'
import { setupLayouts } from "virtual:generated-layouts"
import generatedRoutes from 'virtual:generated-pages'

const router = createRouter({
    // history: createWebHistory(),    // 使用history模式
    history: createWebHashHistory(), // 使用hash模式
    routes:
        // ...constantRoutes
        setupLayouts(generatedRoutes)
    ,
});

export default router;