const mainRouter = [
    {
        path: '/',
        name: 'main',
        // component: () => import('@/Layout/Main.vue'),
        redirect: { name: 'home' },
        meta: { title: "首页" },
        children: [
            {
                name: 'home',
                path: '/home',
                component: () => import('@/views/home.vue'),
            }
        ],
    },
]
export  default  mainRouter
