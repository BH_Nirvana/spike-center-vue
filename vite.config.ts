import { defineConfig, loadEnv } from 'vite'
import Icons from 'unplugin-icons/vite'


// vite plugin插件配置
import {createVitePlugins} from './vite/plugins'


import path from 'path'
import fs from 'fs'

export default ({ mode, command }) =>{

  //获取环境变量
  const env = loadEnv(mode, process.cwd())
  
  // 全局 scss 资源
  const scssResources = []
  fs.readdirSync('src/styles/resources').map(dirname => {
      if (fs.statSync(`src/styles/resources/${dirname}`).isFile()) {
          scssResources.push(`@use "src/styles/resources/${dirname}" as *;`)
      }
  })
  return defineConfig({

    //公共基础路径 base
    //如果你需要在嵌套的公共路径下部署项目，只需指定 base 配置项，然后所有资源的路径都将据此配置重写。
    // base: './',

    //本地运行配置，以及反向代理配置
    server: {
      host: "0.0.0.0",
      https: false, //是否启用 http 2
      cors: true, //为开发服务器配置 CORS , 默认启用并允许任何源
      open: true, //服务启动时自动在浏览器中打开应用
      port: 9000,
      strictPort: false, //设为true时端口被占用则直接退出，不会尝试下一个可用端口
      force: true, //是否强制依赖预构建
      hmr: true, //禁用或配置 HMR 连接
      // 传递给 chockidar 的文件系统监视器选项
      watch: {
        ignored: ["!**/node_modules/your-package-name/**"],
      },
      // 反向代理配置
      proxy: {
        '/dev-api': {
          target:
            mode === 'development'
              ? loadEnv(mode, process.cwd()).VITE_APP_DEV_URL
              : loadEnv(mode, process.cwd()).VITE_APP_PROD_URL,
          changeOrigin: true,
          // logLevel: 'debug',
          rewrite: path => path.replace(/^\/dev-api/, '')
        }
      },
    },
    //让vite在启动之初就对某些资源进行预打包，尽量避免后续的动态打包，
    optimizeDeps: {
      include: [
          'element-plus/es',
          'element-plus/es/components/message/style/css',
          'element-plus/es/components/notification/style/css',
          'element-plus/es/components/message-box/style/css'
      ]
    },
    plugins: [
      createVitePlugins(env, command === 'build'),
      Icons({
        autoInstall: true,
      }),
    ],


    resolve: {
      /** 
       * 添加alias规则，需要引入"@types/node，不然path无法找到
      */
      alias: {
        '@': path.resolve(__dirname, 'src')
      },
      extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json']
    },

    //css相关
    css: {
      // 配置 css modules 的行为
      modules: {},
      // postCss 配置
      postcss: {},
      //指定传递给 css 预处理器的选项
      preprocessorOptions: {
        scss: {
          additionalData: scssResources.join('')
        },
      },
    },
  })
}

